**LibreOffice Writer Template system**

This package allow the creation of a ".odt" file starting from an existing file.
It will substitute the "tags" you inserted in the initial template

Look at the example in the code: it read the file "test_source.odt" and substitute every word that starts with "$"

It can populate tables; in order to do it, the tables must have a name (look at their properties)

IMPORTANT: every tag must have one only formatting style. If the first part is red and the second is green the odtmanager will not be able to find it.
To be sure to have the same style you have to select the tag (every tag) with LibreOffice and then click CTRL+m; now we are sure the tag hasn't any style (neither partial, for half tag).
This is important: writing "$new_ag" and coming back to add the "t" (=>"$new_tag") will generate another -hidden- style for the "t" char. It can be delete with CTRL-m on all the word