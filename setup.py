#!/usr/bin/env python
import setuptools
from setuptools import setup


setup(name='OdtManager',
      version='0.1',
      description='LibreOffice Odt Manager',
      author='Alessandro Tufi',
      author_email='dedalus2000@gmail.com',
      # url=',
      py_modules=['odtmanager'],
      license='MIT',
     # long_description=open('README.txt').read()
     )
