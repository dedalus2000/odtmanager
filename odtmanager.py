# -*- coding: UTF-8 -*-

from __future__ import print_function
from builtins import str
from builtins import range
from builtins import object
from babel.dates import format_date, format_time
from babel.numbers import format_decimal
from collections import OrderedDict
from odf.opendocument import load
from odf import text, table
from odf import teletype
import datetime
import decimal
import copy
import sys
import re


mm = re.compile(r'\$[\w\|]+')


def get_price_filter(zero=2):
    """Convert Decimal to a money formatted string.
       No round is done, please provide it by your own
    """
    def prices(value, ctx):
        if value is None or value == '':
            return ''
        return format_decimal(value, format='#,##0.' + ('0'*zero) + ("#"*10), locale=ctx['lang'])
    return prices



class DocumentObj(object):
    def __init__(self, odt_tree, context=None): #, lang="eng"):
        self.odt_tree = odt_tree  # loaded template
        self.context = context
    
    def convert2unicode(self, obj):
        converter = self.context['toUnicode'].get(type(obj), None)
        if converter is None:
            return str(obj)
        return converter(obj, self.context)

    def subs_tags(self, diz, node=None, clear_notfound=True):
        if node is None:
            node = self.odt_tree

        def subst(node, diz):
            strtext = node.data
            flag = False
            founds = list(set(mm.findall(strtext)))
            founds.sort(reverse=True) # ID_DOC must be managed after ID_DOC_REV

            for found_word in founds:
                # we have found a word in the form "$word[|sdf]"

                _tmp = found_word[1:].split('|')
                tag, filters = _tmp[0], _tmp[1:]
                
                new_word = diz.get(tag)
                
                if new_word is None:
                    if not clear_notfound:
                        continue
                    else:
                        new_word = ''

                try:
                    for filter_ in filters:
                        func = self.context['filters'][filter_]
                        new_word = func(new_word, self.context)
                except Exception as err:
                    print("Error: %s" % err, new_word, tag, filters, file=sys.stderr)
                    new_word = "[ERROR]"

                flag = True
                strtext = strtext.replace(found_word, self.convert2unicode(new_word))

            if flag:
                # almost one substitution has been made
                ss=text.Span()
                parent = node.parentNode
                parent.insertBefore(ss, node)
                parent.removeChild(node)
                node.parentNode=None # serve?
                teletype.addTextToElement(ss, strtext)

        def pp(node, diz):
            if node is None:
                return
            if hasattr(node, 'data'):
                subst(node, diz)
            pp(node.nextSibling, diz)
            for nn in node.childNodes: # FIX deve cambiare in un while
                pp(nn, diz)

        for paragraph in node.getElementsByType(text.P): # da migliorare
            pp(paragraph, diz)

        return node

    def _clone(self, node, parent=None, prev=None):
        new_node = copy.copy(node)
        new_node.parentNode = parent
        new_node.nextSibling = None
        new_node.previousSibling = prev

        new_node.childNodes = []

        prev = None
        for nn in node.childNodes:
            new_node.childNodes.append( self._clone(nn, new_node, prev))
            prev = nn

        prev = None
        for ii in range(0, len(node.childNodes),-1):
            nn = node.childNodes[ii]
            nn.nextSibling = prev
            prev = nn
        return new_node

    def subs_single_table(self, data_tbl, tb_node):
        rows = data_tbl['rows']
        first_row = data_tbl.get('rowStart', 1)

        # prendo la riga di "template" e la memorizzo
        # FIX: first_row può eccedere la lunghezza dei nodi!
        row_tpl = tb_node.getElementsByType(table.TableRow)[first_row]

        for doc_row in rows:
            doc_row = dict(doc_row.value if hasattr(doc_row, 'value') else doc_row)
            #row = copy.deepcopy(row_tpl)
            row = self._clone(row_tpl)
            self.subs_tags(doc_row, row, clear_notfound=False)
            tb_node.insertBefore(row, row_tpl)

        # rimuovo dall'albero la riga di "template"
        row_tpl.parentNode.childNodes.remove(row_tpl)

    def subs_tables(self, data_tables):
        for ii,tb_node in enumerate(self.odt_tree.getElementsByType(table.Table)):
            name = tb_node.getAttribute('name')
            if name in data_tables:
                self.subs_single_table(data_tables[name], tb_node)

    def save(self, filepath):
        self.odt_tree.save(filepath if isinstance(filepath, str) else filepath.decode('utf8'))
        return filepath

def _merge_dict(to_, from_):
    for kk,vv in list(from_.items()):
        if isinstance(vv, dict):
            _merge_dict(to_.setdefault(kk, {}), from_[kk])
        else:
            to_[kk] = vv


def manage(tmpl_fname, tags, context=None):
    """ This is the most important function: it can help you to create an .odt file

        tmpl_fname : odt template file name 
        tags : dictionary TAG<=>CONTENTS. Note that tables are represented as
                nested dictionary:
                ...
                "tablename": {
                    "rows": [
                        {"col1":val1, "col2":val2},  # row 1
                        ...
                    ],
                "rowStart": 0  # this is the row number since where to start 
                               # to insert the new ones.
                               # it not necessary: default is 1
                }
                ...
    """

    data_tables = {}
    heading_dict = {}

    for kk, vv in list(tags.items()):
        if hasattr(vv, 'get') and vv.get('rows', None) is not None:
            data_tables[kk] = vv
        else:
            heading_dict[kk] = vv

    if not isinstance(tmpl_fname, str):
        tmpl_fname = tmpl_fname.decode('utf8')

    odt_tpl = load(tmpl_fname)
    
    def date_formatter(obj, ctx):
        dd = format_date(obj, format='long', locale=ctx['lang'])
        return u' '.join([x.capitalize() for x in dd.split(' ')])

    def datetime_formatter(obj, ctx):

        if obj.tzinfo:
            return u"[TO BE DONE]"
        dt = format_time(obj, "hh:mm a", locale=ctx['lang'])
        dd = date_formatter(obj, ctx)
        return u'%s  %s' % (dd, dt)

    default_context = dict(
        toUnicode = OrderedDict({
            None : lambda obj,ctx: u'',
            str : lambda obj,ctx: obj.decode('utf8', errors='ignore'),
            float : lambda obj,ctx: str(decimal.Decimal(str(obj))),
            decimal.Decimal : lambda obj,ctx: str(obj),
            datetime.datetime : datetime_formatter,
            datetime.date : date_formatter,
        }),
        filters = {
            "P": get_price_filter(), #**kwargs),
           # 'S': get_strip_filter()
        },
        lang = 'en'
    )
    if context:
        _merge_dict(default_context, context)
    
    doc = DocumentObj(odt_tpl, context=default_context)
    
    doc.subs_tables(data_tables)
    doc.subs_tags(heading_dict)
    return doc  #  doc.save(filename) to save the template to disk


if __name__=='__main__':

    # preparing the tags dictionary
    now = datetime.datetime.now()
    lang = 'it'

    head = {}
    head['Tag1'] = 'test1'
    head['Tag2'] = 'test2'
    head['dd_now'] = now.date()
    head['dt_now'] = now
    head['lang'] = lang
    head['none'] = None
    head['float'] = 123456789.123456789
    head['decimal'] = decimal.Decimal("123456789.123456789")
    head['str'] = "String with strange chars: §ùç\n..with a new row\n\t..with tab\n..and with some 'fake' xml tags: <b>this should not be bold</b>"

    head['price1'] = 123456789.1
    head['price2'] = 123456789.123456789
    head['discount1'] = 9
    head['discount2'] = 9.12345

    # table: in the odt template the name of the table must be set into its proprerties
    table1 = dict(
        rows=[
            dict(col1='cell31', col2='cell32'),
            dict(col1='cell11', col2='cell12'),
            dict(col1='cell21', col2='cell22'),
        ],
        rowStart=2
    )
    
    head['table1'] = table1

    def strip_filter(value, ctx):
        depth = 2
        return format_decimal(value, format='0.' + "0"*depth + "##", locale=ctx['lang'])
    
    # overwriting and adding few context variables
    ctx = dict(
        lang=lang,  # overwriting the "lang"
        filters = dict(
            S=strip_filter  # add a new filter
        )
    )

    # creating and saving the template
    manage(u'test_source.odt', head, context=ctx).save('out.odt')
    print("'out.odt' saved")
